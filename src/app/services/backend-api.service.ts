import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class BackendApiService {

  private userBaseUrl: string = 'https://baseUrl/business';
  private groupsBaseUrl: string = 'https://baseUrl/groups';
  private projectBaseUrl: string = 'https://baseUrl/projects';
  public projectData;

  public updateUserList = new Subject<any>();
  public refreshLoggedUserData = new Subject<any>();
  public getGroupsList = new Subject<any>();
  public usersGroupUpdate = new Subject<any>();

  constructor(
    private http: HttpClient,
    private titleService: Title
  ) { }

  getProjectData(){
    var promise = new Promise ((resolve, reject) => {
      const projectId = '5d4c07fb030f5d0600bf5c03';
      return this.http.get(this.projectBaseUrl + "/getProject/:" + projectId).subscribe(
         (projectData: any) => {
            this.projectData = projectData;
            this.titleService.setTitle( this.projectData.metaTitle );
            resolve(projectData);
         } 
      );
    });
    return promise;
  }

  loginApiRequest(loginData: any) {
    var promise = new Promise((resolve, reject) => {
      return this.http.post(this.userBaseUrl + "/login", loginData).subscribe(
        (backendResponse: any) => {
          resolve(backendResponse);
        }
      );
    });
    return promise;
  }

  userAddRequest(userData: any, userImage: File, loggedUserId: number) {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');

    var fd = new FormData();
    fd.append('userData', JSON.stringify(userData));
    fd.append('loggedUserId', JSON.stringify(loggedUserId));

    if (userImage != null)
      fd.append('file', userImage, userImage.name);

    var promise = new Promise((resolve, reject) => {
      return this.http.post(this.userBaseUrl + "/adduser", fd, { headers: headers }).subscribe(
        (backendResponse: any) => {
          resolve(backendResponse);
          this.updateUserList.next(backendResponse.users);
        }
      );
    });
    return promise;
  }

  userUpdateRequest(userData: any, userImage: File, loggedUserId: number) {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');

    var fd = new FormData();
    fd.append('userData', JSON.stringify(userData));

    if (userImage != null)
      fd.append('file', userImage, userImage.name);

    var promise = new Promise((resolve, reject) => {
      return this.http.post(this.userBaseUrl + "/updateuser", fd, { headers: headers }).subscribe(
        (backendResponse: any) => {
          resolve(backendResponse);
        })
    });
    return promise;
  }

  getUsersRequest(loggedinUserId: number) {
    const data = { '_id': loggedinUserId, projectId: '5d4c07fb030f5d0600bf5c03'}
    return this.http.post(this.userBaseUrl + "/getusers", data).subscribe(
      (usersList: any) => {
        this.updateUserList.next(usersList);
      }
    );
  }

  getLoggedInUserRequest(email: string) {
    const data = { 'email': email, 'projectId': "5d4c07fb030f5d0600bf5c03" };
    return this.http.post(this.userBaseUrl + '/getloggeduser', data).subscribe(
      (loggedUserData: any) => {
        this.refreshLoggedUserData.next(loggedUserData);
      }
    );
  }

  deleteUserRequest(userId: number, myUserId) {
    const data = { 'userId': userId, '_id': myUserId, projectId: '5d4c07fb030f5d0600bf5c03' }

    var promise = new Promise((resolve, reject) => {
      return this.http.post(this.userBaseUrl + "/deleteuser", data).subscribe(
        (updatedUsersList: any) => {
          resolve(updatedUsersList);
          this.updateUserList.next(updatedUsersList);
        }
      );
    });
    return promise;
  }

  // ************************* ********************************************* */
  // ************************* GROUPS ***************************************** */
  // ************************* ********************************************* */

  createGroup(groupName: string) {
    const data = { 'name': groupName, projectId: '5d4c07fb030f5d0600bf5c03' };
    var promise = new Promise((resolve, reject) => {
      return this.http.post(this.groupsBaseUrl + "/creategroup", data).subscribe(
        (backendResponse: any) => {
          resolve(backendResponse);
          this.getGroupsList.next(backendResponse);
        }
      );
    });
    return promise;
  }

  editGroup(groupId: number, groupName: string) {
    const data = { 'groupId': groupId, 'groupName': groupName, projectId: '5d4c07fb030f5d0600bf5c03' };

    return this.http.post(this.groupsBaseUrl + '/editgroup', data).subscribe(
      (backendResponse) => {
        this.getGroupsList.next(backendResponse);
      });
  }

  deleteGroup(groupId: number) {
    const data = { 'groupId': groupId, 'status': 0, projectId: '5d4c07fb030f5d0600bf5c03' }
    return this.http.post(this.groupsBaseUrl + "/deletegroup", data).subscribe(
      (backendResponse) => {
        this.getGroupsList.next(backendResponse);
      }
    )
  }

  getGroups() {
    const projectId = '5d4c07fb030f5d0600bf5c03';
    return this.http.get(this.groupsBaseUrl + "/getgroups/"+projectId).subscribe(
      (groupsList: any) => {
        this.getGroupsList.next(groupsList);
      }
    )
  };

  addUsersInGroups(user: number, selectedGroupId: number) {
    const data = { 'user': user, 'selectedGroupId': selectedGroupId, projectId: '5d4c07fb030f5d0600bf5c03' };
    return this.http.post(this.groupsBaseUrl + "/addusergroup", data).subscribe(
      (backendResponse) => {
        this.usersGroupUpdate.next(backendResponse);
      }
    )
  }

  deleteUserInGroup(selectedUser: number, selectedGroupId: number) {
    const data = { 'user': selectedUser, 'selectedGroupId': selectedGroupId, projectId: '5d4c07fb030f5d0600bf5c03' };
    return this.http.post(this.groupsBaseUrl + "/deletegroupuser", data).subscribe(
      (backendResponse) => {
        this.usersGroupUpdate.next(backendResponse);
      }
    )
  }

  getAddedUsers(groupId: number) {
    const data = { 'selectedGroupId': groupId , projectId: '5d4c07fb030f5d0600bf5c03'}
    return this.http.post(this.groupsBaseUrl + "/getaddedusers", data).subscribe(
      (groupUsers) => {
        this.usersGroupUpdate.next(groupUsers);
      }
    )
  }

} // ---- CLASS ENDS ------
